/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["dist/app.58a62140180335de7dc6.js","8101216a6bf993c9865c93156fec503a"],["dist/app.58a62140180335de7dc6.js.map","dbbe51e0975d13cd008368a55c30db6f"],["dist/core-service-worker.js","97d96ad2197c46d761d9fdd8735798c6"],["dist/core-service-worker.js.map","9639aece9f8b7218cbc5146710dab251"],["dist/dayjs-locales0.58a62140180335de7dc6.js","0e7d3af59d320a566823035ae327f8a1"],["dist/dayjs-locales0.58a62140180335de7dc6.js.map","f0c9ca905e1bc5248b3c887ae14fc25e"],["dist/dayjs-locales10.58a62140180335de7dc6.js","214cf376786b929138a80dba11c0312f"],["dist/dayjs-locales10.58a62140180335de7dc6.js.map","f19b8a7b84e71833c77706412ee0e2d5"],["dist/dayjs-locales100.58a62140180335de7dc6.js","d7e6a39d2a29f52686f0a5fecd159dda"],["dist/dayjs-locales100.58a62140180335de7dc6.js.map","a152b69fd80ff304ba0c648cb2dd8058"],["dist/dayjs-locales102.58a62140180335de7dc6.js","99105164610241b14b4cb0e847843251"],["dist/dayjs-locales102.58a62140180335de7dc6.js.map","6f51f3d069640725151027c75b041579"],["dist/dayjs-locales104.58a62140180335de7dc6.js","302712e654d08d983866d94992dde81e"],["dist/dayjs-locales104.58a62140180335de7dc6.js.map","dce6a5139cad93e72d66c9e5484ae037"],["dist/dayjs-locales106.58a62140180335de7dc6.js","adfc904a64d2a390f5639f868c357a4e"],["dist/dayjs-locales106.58a62140180335de7dc6.js.map","d88b1d951e50b7234e1e812460819943"],["dist/dayjs-locales108.58a62140180335de7dc6.js","498c11d18582aa1c274dbde0d902a61d"],["dist/dayjs-locales108.58a62140180335de7dc6.js.map","131cbe1c6aa93fc983575798912a8afb"],["dist/dayjs-locales110.58a62140180335de7dc6.js","dddcfa2d27240533aad2a830de927df0"],["dist/dayjs-locales110.58a62140180335de7dc6.js.map","d06034f16ea640ba75907570a8608647"],["dist/dayjs-locales112.58a62140180335de7dc6.js","f5674d3c4d714aa86f32ab670352f558"],["dist/dayjs-locales112.58a62140180335de7dc6.js.map","9bf9496fc9460eea0702f1991e8d9830"],["dist/dayjs-locales114.58a62140180335de7dc6.js","ba7fefe3768baf5f1ce6cb9fc372cd2b"],["dist/dayjs-locales114.58a62140180335de7dc6.js.map","285a08ce5967718ae5cca8a3d80a7db7"],["dist/dayjs-locales116.58a62140180335de7dc6.js","431f919b6fa40b7ed546426558136dee"],["dist/dayjs-locales116.58a62140180335de7dc6.js.map","5c8a1e3310ed0067188d271dbb69ad23"],["dist/dayjs-locales118.58a62140180335de7dc6.js","6183dc7a9e3d43bda3fb07ed44b5f6f1"],["dist/dayjs-locales118.58a62140180335de7dc6.js.map","5f6e307fd3a7a7cb2c1fdef821eaa5d1"],["dist/dayjs-locales12.58a62140180335de7dc6.js","0f925e2ba0a47824e78f02d56a36053b"],["dist/dayjs-locales12.58a62140180335de7dc6.js.map","6538a40628c1adfebe1307e1c24fc3da"],["dist/dayjs-locales120.58a62140180335de7dc6.js","9ec9b765ab63c3af70ff1f88e557238f"],["dist/dayjs-locales120.58a62140180335de7dc6.js.map","bb1a25fe50bd2fc9c24fcf22b9d609ef"],["dist/dayjs-locales122.58a62140180335de7dc6.js","fcc6bed97c829f90eff050ae91434abc"],["dist/dayjs-locales122.58a62140180335de7dc6.js.map","9a5ff2057b9adc5007eca0a4bcd093ba"],["dist/dayjs-locales124.58a62140180335de7dc6.js","7b45f05a92fdf5f99143109e00f3c399"],["dist/dayjs-locales124.58a62140180335de7dc6.js.map","0504756b6bf0be29d8b160790d20a95d"],["dist/dayjs-locales126.58a62140180335de7dc6.js","007ed70463d601e448a502cfc10778fc"],["dist/dayjs-locales126.58a62140180335de7dc6.js.map","ccd60b59ea704cd7f2d675b672190f56"],["dist/dayjs-locales128.58a62140180335de7dc6.js","673423c93979b17ad5907db087270aea"],["dist/dayjs-locales128.58a62140180335de7dc6.js.map","37d29e3250b93e85fa57a33dfa6bd0bd"],["dist/dayjs-locales130.58a62140180335de7dc6.js","461277d805a88e21163fe8c969f2b707"],["dist/dayjs-locales130.58a62140180335de7dc6.js.map","d2e692cd1c775f7acd51c60810f533f2"],["dist/dayjs-locales132.58a62140180335de7dc6.js","d652a5d9a09afe03ec53caef456a4ad4"],["dist/dayjs-locales132.58a62140180335de7dc6.js.map","fe95bf3ed2eed62986df667b657a1c04"],["dist/dayjs-locales134.58a62140180335de7dc6.js","5c161ba3bd06ca229a4a83b5cf10306c"],["dist/dayjs-locales134.58a62140180335de7dc6.js.map","00b2b64a88a9a11981ed9e73e4a0976a"],["dist/dayjs-locales136.58a62140180335de7dc6.js","49552a3793fb1cf5ecc167a42e084d44"],["dist/dayjs-locales136.58a62140180335de7dc6.js.map","68f146955848d03c577a3f6a39da26cd"],["dist/dayjs-locales138.58a62140180335de7dc6.js","f5c5466e0d05aad7936408da9231b48b"],["dist/dayjs-locales138.58a62140180335de7dc6.js.map","c7940e29765e97899f75c8d93c637457"],["dist/dayjs-locales14.58a62140180335de7dc6.js","3fe79364c0613ba2bbbd9e6b9e8295ec"],["dist/dayjs-locales14.58a62140180335de7dc6.js.map","545c33e1158c2939381664a683304db2"],["dist/dayjs-locales140.58a62140180335de7dc6.js","9f86a4bdd7efe3455ebe89ba60b2e3a0"],["dist/dayjs-locales140.58a62140180335de7dc6.js.map","6a24cf8e8fb944cc8e3595821e6bf08e"],["dist/dayjs-locales142.58a62140180335de7dc6.js","7a71ecef8bccd61491eb730a92cd0be5"],["dist/dayjs-locales142.58a62140180335de7dc6.js.map","4f00eadac55971bd72e542e9fc2843fa"],["dist/dayjs-locales144.58a62140180335de7dc6.js","702c999415809dcc2b914f4394ffcf1a"],["dist/dayjs-locales144.58a62140180335de7dc6.js.map","8c9db1c42ef84443a65e527af4b00013"],["dist/dayjs-locales146.58a62140180335de7dc6.js","3e3ff44b4bde3d6148102f0ac8a5f732"],["dist/dayjs-locales146.58a62140180335de7dc6.js.map","d83e798dba498b73e94b16ea4d4c4c40"],["dist/dayjs-locales148.58a62140180335de7dc6.js","e69cf6441e4a3dc783a560494c67773d"],["dist/dayjs-locales148.58a62140180335de7dc6.js.map","c4e5e322a46e15eb5e23587052a1dd3d"],["dist/dayjs-locales150.58a62140180335de7dc6.js","fff72f45082aec485a97a98041dbe412"],["dist/dayjs-locales150.58a62140180335de7dc6.js.map","945d95164d47fd0472e8cb017e43cc04"],["dist/dayjs-locales152.58a62140180335de7dc6.js","a2ac194ef28b2c20b0ae34f5f016b15f"],["dist/dayjs-locales152.58a62140180335de7dc6.js.map","21af09bfe68160f13aaf3461a7da0c54"],["dist/dayjs-locales154.58a62140180335de7dc6.js","e97cca890fba02789b6df32c093865fd"],["dist/dayjs-locales154.58a62140180335de7dc6.js.map","a7e279f63428f2ece1d0663afa37d088"],["dist/dayjs-locales156.58a62140180335de7dc6.js","9d631c3ca3e913bf2428bd447863091a"],["dist/dayjs-locales156.58a62140180335de7dc6.js.map","14b00f37edd6e242290b3b9cd276aa1e"],["dist/dayjs-locales158.58a62140180335de7dc6.js","ec8381e194332268af0da1817f2309a2"],["dist/dayjs-locales158.58a62140180335de7dc6.js.map","05aff37b67a0bf37ec6cad11b5f24c57"],["dist/dayjs-locales16.58a62140180335de7dc6.js","453bbaeb522690c42c0b2dd7d4c6bc3a"],["dist/dayjs-locales16.58a62140180335de7dc6.js.map","de47e730beca1a0e4fe729c35957bb4a"],["dist/dayjs-locales160.58a62140180335de7dc6.js","f5f7cf707a80b079b8fc9cb42e6b4f0c"],["dist/dayjs-locales160.58a62140180335de7dc6.js.map","271ed3a2603cededc016f4bdea5fcf7f"],["dist/dayjs-locales162.58a62140180335de7dc6.js","06b482a95ab1b8707dd767c6bc889dec"],["dist/dayjs-locales162.58a62140180335de7dc6.js.map","0b259b87fe6acf5c8dac18f0fbfd442d"],["dist/dayjs-locales164.58a62140180335de7dc6.js","93d1a08a378f6aa655c8ed4c4033fcf2"],["dist/dayjs-locales164.58a62140180335de7dc6.js.map","7100e259a859a7527eaf309ffec0d3d6"],["dist/dayjs-locales166.58a62140180335de7dc6.js","ffe51082a9aeb9c737de950d31583197"],["dist/dayjs-locales166.58a62140180335de7dc6.js.map","7fe2b284c4b40a7ac8bac959018182d7"],["dist/dayjs-locales168.58a62140180335de7dc6.js","8464dbc2f86de797cbc5587d5853eeca"],["dist/dayjs-locales168.58a62140180335de7dc6.js.map","8743447a4760e3d2e2772ae2eedc0cc2"],["dist/dayjs-locales170.58a62140180335de7dc6.js","edb0597b1b94db6369628c1bcff3beda"],["dist/dayjs-locales170.58a62140180335de7dc6.js.map","9836d7f8113b25e920803782a74dd88b"],["dist/dayjs-locales172.58a62140180335de7dc6.js","14bd5605687a2a24734f4d849f1ca2ba"],["dist/dayjs-locales172.58a62140180335de7dc6.js.map","cdf76f0c6b7322715ad1fe28e830d5e0"],["dist/dayjs-locales174.58a62140180335de7dc6.js","bcbc06630855b9f4e58eb77066e49fd0"],["dist/dayjs-locales174.58a62140180335de7dc6.js.map","996648c2889850cec720c6ddc5baa291"],["dist/dayjs-locales176.58a62140180335de7dc6.js","c5e654a190351c4e99dbf64845a7f930"],["dist/dayjs-locales176.58a62140180335de7dc6.js.map","f95a4f5a11bd52a31e1fd265c1b4fc7e"],["dist/dayjs-locales178.58a62140180335de7dc6.js","607ee55cf07ca01b3b0e1cb44031b8b1"],["dist/dayjs-locales178.58a62140180335de7dc6.js.map","1a6b391a414211d21beb7bd063d0273c"],["dist/dayjs-locales18.58a62140180335de7dc6.js","8645bd1aeb9af26a14af46b63cb00c22"],["dist/dayjs-locales18.58a62140180335de7dc6.js.map","bb9ee2d1bd26f412d7e97110b3e93d37"],["dist/dayjs-locales180.58a62140180335de7dc6.js","28c6e9cc47f3005f6dabae577d99f8c1"],["dist/dayjs-locales180.58a62140180335de7dc6.js.map","34c5cc5c92fe09ade53d9f1ce688718d"],["dist/dayjs-locales182.58a62140180335de7dc6.js","8a9b02d2adc68e979447f65061b57abf"],["dist/dayjs-locales182.58a62140180335de7dc6.js.map","79f4e108cc43b9b52d6f8f45d4675a82"],["dist/dayjs-locales184.58a62140180335de7dc6.js","aa0464e47984bf9e22effd10ed24b3ab"],["dist/dayjs-locales184.58a62140180335de7dc6.js.map","9eba0bda3e786c161334c5f195a32657"],["dist/dayjs-locales186.58a62140180335de7dc6.js","cfe9e37f1cb9d8bb56c463bffe6c26d0"],["dist/dayjs-locales186.58a62140180335de7dc6.js.map","de412f019a0da225a1b9014acdd8b4a4"],["dist/dayjs-locales188.58a62140180335de7dc6.js","931862e60131ec7758acebf3e561dbca"],["dist/dayjs-locales188.58a62140180335de7dc6.js.map","da3e76619849f995c023cb10417d5241"],["dist/dayjs-locales190.58a62140180335de7dc6.js","1537ee4a6229acbabc65cab4eae095ab"],["dist/dayjs-locales190.58a62140180335de7dc6.js.map","8c191b70ba6d40325c5b0f45613773dd"],["dist/dayjs-locales192.58a62140180335de7dc6.js","a3eaf0a38f0ff067c71d87c5064c0344"],["dist/dayjs-locales192.58a62140180335de7dc6.js.map","3e78983b9af2bec635ede67f9b54cd90"],["dist/dayjs-locales194.58a62140180335de7dc6.js","6f21aeea1ad75e9b774a9c0af6fd2a56"],["dist/dayjs-locales194.58a62140180335de7dc6.js.map","96a63e10dcedef41630afd7f95346eaa"],["dist/dayjs-locales196.58a62140180335de7dc6.js","f09b0ebf0962f8067dfc1c23d81180b1"],["dist/dayjs-locales196.58a62140180335de7dc6.js.map","4000dec5ae889fbc372567294365f5ab"],["dist/dayjs-locales198.58a62140180335de7dc6.js","200230cda1b97806ed68a373cb893015"],["dist/dayjs-locales198.58a62140180335de7dc6.js.map","830a62ac59884edc187f0b4a4dd8ffe5"],["dist/dayjs-locales2.58a62140180335de7dc6.js","8b929bf0ea155e8c87fee8587f4860f0"],["dist/dayjs-locales2.58a62140180335de7dc6.js.map","8b77b18f456eb184487fc380b678a4c9"],["dist/dayjs-locales20.58a62140180335de7dc6.js","6c0a7ce015c5086c1998ca639dded0eb"],["dist/dayjs-locales20.58a62140180335de7dc6.js.map","e6bc0e9fb4eaf571e8b63842f732cee8"],["dist/dayjs-locales200.58a62140180335de7dc6.js","c3607db855cfcef5bf1f67fa3d264405"],["dist/dayjs-locales200.58a62140180335de7dc6.js.map","4223ca08bc41bc6ae0e1e89a8f292e43"],["dist/dayjs-locales202.58a62140180335de7dc6.js","08d339cb64f811200f6f5f3469775536"],["dist/dayjs-locales202.58a62140180335de7dc6.js.map","23e32ead039f5f08843c4f9dac2479f5"],["dist/dayjs-locales204.58a62140180335de7dc6.js","0d0374179298043dc21cc9baebc22da8"],["dist/dayjs-locales204.58a62140180335de7dc6.js.map","2cc6da729482a435d530e8e3eb134613"],["dist/dayjs-locales206.58a62140180335de7dc6.js","30d585f649894ff634d916c44b5cdd87"],["dist/dayjs-locales206.58a62140180335de7dc6.js.map","eb81f2c4aaa355c2ebeae4ae9462809a"],["dist/dayjs-locales208.58a62140180335de7dc6.js","565589fab4b40e2b38af1ba1fe833246"],["dist/dayjs-locales208.58a62140180335de7dc6.js.map","c5c44b47a2bec82657b2ed84131bda19"],["dist/dayjs-locales210.58a62140180335de7dc6.js","c3312e27dfd7b6cc951eaf1a513c7d0e"],["dist/dayjs-locales210.58a62140180335de7dc6.js.map","577e64768a6e786e09e844e621667678"],["dist/dayjs-locales212.58a62140180335de7dc6.js","578c85cb48fb9ca6fba14fee5b51f028"],["dist/dayjs-locales212.58a62140180335de7dc6.js.map","2277db39ce48514b1777355578611e21"],["dist/dayjs-locales214.58a62140180335de7dc6.js","3d2157cd862c140893ae1d451763f4d0"],["dist/dayjs-locales214.58a62140180335de7dc6.js.map","599a9495055194e9b2d6c5199cdacfce"],["dist/dayjs-locales216.58a62140180335de7dc6.js","6564665b268831e22809c00209e5ec00"],["dist/dayjs-locales216.58a62140180335de7dc6.js.map","db069349f6f6e2252b2ff86b1a4e3861"],["dist/dayjs-locales218.58a62140180335de7dc6.js","9c28025f5666fe579b9649f36abec871"],["dist/dayjs-locales218.58a62140180335de7dc6.js.map","43b99e2e2bcd0ac0e99d1508ea6dd23e"],["dist/dayjs-locales22.58a62140180335de7dc6.js","a8914787598ca7886df75e97445826d9"],["dist/dayjs-locales22.58a62140180335de7dc6.js.map","93a267b18cfcbab38651885bde080dd8"],["dist/dayjs-locales220.58a62140180335de7dc6.js","433d6229867de8ba5079637b6421855e"],["dist/dayjs-locales220.58a62140180335de7dc6.js.map","2a69361359a19a218c270775505cfdd4"],["dist/dayjs-locales222.58a62140180335de7dc6.js","de128528920f1aea960b0b215da9b133"],["dist/dayjs-locales222.58a62140180335de7dc6.js.map","5f0735632a9894eeeb03bc260edacaaa"],["dist/dayjs-locales224.58a62140180335de7dc6.js","de1676269d959fbd2a4803d4fee60b8e"],["dist/dayjs-locales224.58a62140180335de7dc6.js.map","ccf9ce34fb1f418ae3ca585207c78e70"],["dist/dayjs-locales226.58a62140180335de7dc6.js","d386fb6ec794307f8735ff3a69c1c8d5"],["dist/dayjs-locales226.58a62140180335de7dc6.js.map","81b078cded11a08aa8f089a3966a45b4"],["dist/dayjs-locales228.58a62140180335de7dc6.js","e1068f3d4f90d2fce25fbe7714933ce8"],["dist/dayjs-locales228.58a62140180335de7dc6.js.map","1c856807b26e596334d3d7dc2107490a"],["dist/dayjs-locales230.58a62140180335de7dc6.js","0031fd7dab81d2b9e1d1a3d6ebeea198"],["dist/dayjs-locales230.58a62140180335de7dc6.js.map","f1e66962cec457c3046286aa9be9b5e0"],["dist/dayjs-locales232.58a62140180335de7dc6.js","fe48b488db3212522b3b6d8e0727b9d8"],["dist/dayjs-locales232.58a62140180335de7dc6.js.map","3745925ee0ec324a59a798c7cb0a074e"],["dist/dayjs-locales234.58a62140180335de7dc6.js","e98e4ca37ca39d1439adba86a4475f35"],["dist/dayjs-locales234.58a62140180335de7dc6.js.map","86878db5437ccfd49114af1285b169d0"],["dist/dayjs-locales236.58a62140180335de7dc6.js","e2f0be1077474ad7bcd597c6047612ce"],["dist/dayjs-locales236.58a62140180335de7dc6.js.map","316b2c76187a919dab48d9681e4e36ec"],["dist/dayjs-locales238.58a62140180335de7dc6.js","88156c762dfdc17abc034d9fd82d6d6a"],["dist/dayjs-locales238.58a62140180335de7dc6.js.map","d0793231bca8442652454251c9a0ba65"],["dist/dayjs-locales24.58a62140180335de7dc6.js","d43dbcdf692904cb8de748ba76183d52"],["dist/dayjs-locales24.58a62140180335de7dc6.js.map","dfc9f4517dec2a60cf8b6f18a1adb1df"],["dist/dayjs-locales240.58a62140180335de7dc6.js","bfc12ff17f7adbd0c6d93fdaba31e38c"],["dist/dayjs-locales240.58a62140180335de7dc6.js.map","2b8c36ee7217e57af4c5cbfb6c5a2b97"],["dist/dayjs-locales242.58a62140180335de7dc6.js","192f03da3655f635b0f020f2ac7cb6e1"],["dist/dayjs-locales242.58a62140180335de7dc6.js.map","4d007ef673d5ce1384b2fa75d16b9e4b"],["dist/dayjs-locales244.58a62140180335de7dc6.js","ca9a9249525409fcad4679a8add91ed4"],["dist/dayjs-locales244.58a62140180335de7dc6.js.map","bdaad373d193bb4b3d5d479f68369dca"],["dist/dayjs-locales246.58a62140180335de7dc6.js","dfcce1f14b7f0b4375efd766ec967277"],["dist/dayjs-locales246.58a62140180335de7dc6.js.map","afff917411279a4e5f0d8ea02507b2d6"],["dist/dayjs-locales248.58a62140180335de7dc6.js","59c30396a959bc193420a74ab216e775"],["dist/dayjs-locales248.58a62140180335de7dc6.js.map","f63c6216671adc3a0fa2bc8859016018"],["dist/dayjs-locales250.58a62140180335de7dc6.js","777d709474ed5016d78d3527f3e2669b"],["dist/dayjs-locales250.58a62140180335de7dc6.js.map","8e22651a8c88ac6f2bcef07d18006903"],["dist/dayjs-locales252.58a62140180335de7dc6.js","6612a506d11de541168c76bbac865a6f"],["dist/dayjs-locales252.58a62140180335de7dc6.js.map","3b580c7d7aaf22d2e5b0f382270f904a"],["dist/dayjs-locales254.58a62140180335de7dc6.js","57f7cb288d7c8bc41627d27e178aec7b"],["dist/dayjs-locales254.58a62140180335de7dc6.js.map","5894171ed9c505083cf3f1065f810098"],["dist/dayjs-locales256.58a62140180335de7dc6.js","1826cd635db5e7ed3bbc7ddb7af92c3c"],["dist/dayjs-locales256.58a62140180335de7dc6.js.map","99f333e376d504ce3ce299e2ce037f80"],["dist/dayjs-locales26.58a62140180335de7dc6.js","2899a54479ababb1607b9591587eec82"],["dist/dayjs-locales26.58a62140180335de7dc6.js.map","5fdf921b6ac7e3e0f6ddc0c2cf006294"],["dist/dayjs-locales28.58a62140180335de7dc6.js","c39dbc75e68adbe3365a533e23915d76"],["dist/dayjs-locales28.58a62140180335de7dc6.js.map","c61d9c04a7c9d41e75ebe8a9cd94f0b2"],["dist/dayjs-locales30.58a62140180335de7dc6.js","3a48c73a991fc293611d89057df50faa"],["dist/dayjs-locales30.58a62140180335de7dc6.js.map","3caa63f21e1b19b87f80929068664f12"],["dist/dayjs-locales32.58a62140180335de7dc6.js","c722a36b35fcd5d4d5ffcae2015b510a"],["dist/dayjs-locales32.58a62140180335de7dc6.js.map","1d2d541e2bd89a89a0db1c1ede911cdf"],["dist/dayjs-locales34.58a62140180335de7dc6.js","90b225c500738615ffd695bb1dbd6d01"],["dist/dayjs-locales34.58a62140180335de7dc6.js.map","066ad7c2f50ab96712eb7f62fc0f92c7"],["dist/dayjs-locales36.58a62140180335de7dc6.js","539bad89b6445e7ee8543f0b63c0bb96"],["dist/dayjs-locales36.58a62140180335de7dc6.js.map","f11d4fc01d89a02fdcf539ad06d082eb"],["dist/dayjs-locales38.58a62140180335de7dc6.js","331dc74f595c2a174af7a7595aafab8e"],["dist/dayjs-locales38.58a62140180335de7dc6.js.map","595e25131b73fedc288f94f4b72afdfa"],["dist/dayjs-locales4.58a62140180335de7dc6.js","6eaf638a3aa473209532b65c429ee29b"],["dist/dayjs-locales4.58a62140180335de7dc6.js.map","ba943f043c389eb81ba2831accc48ce8"],["dist/dayjs-locales40.58a62140180335de7dc6.js","e3f938144994bdfe760605dc13c1e42d"],["dist/dayjs-locales40.58a62140180335de7dc6.js.map","c8043113e9a08e69b3ef57b5788b9863"],["dist/dayjs-locales42.58a62140180335de7dc6.js","37ae898fbaf7c4c87cf0aa82f63b35a6"],["dist/dayjs-locales42.58a62140180335de7dc6.js.map","adceb5700317270a44b01f2442be9463"],["dist/dayjs-locales44.58a62140180335de7dc6.js","3eb4e8ea25fdb3842efe59673876cb4e"],["dist/dayjs-locales44.58a62140180335de7dc6.js.map","3688edfe8a18da1523ead6dadf8bcad8"],["dist/dayjs-locales46.58a62140180335de7dc6.js","d9f9f8b8e1d0513263b920c480bdb02b"],["dist/dayjs-locales46.58a62140180335de7dc6.js.map","353b3eeed705cdfcf70173672b938916"],["dist/dayjs-locales48.58a62140180335de7dc6.js","e9005bafd7c6f7a2cde9359e6e11766c"],["dist/dayjs-locales48.58a62140180335de7dc6.js.map","765bbcf2a3ac4920a2b7bfb4d498668f"],["dist/dayjs-locales50.58a62140180335de7dc6.js","6fc39cd8b746cfa637b060eaba85d925"],["dist/dayjs-locales50.58a62140180335de7dc6.js.map","6a06a091df4f22c30ff5338d0c0f15e9"],["dist/dayjs-locales52.58a62140180335de7dc6.js","636e8f3a402e21378039b474c44dabec"],["dist/dayjs-locales52.58a62140180335de7dc6.js.map","0f6dcfa12cb0e78f2f3a54809c02bdbd"],["dist/dayjs-locales54.58a62140180335de7dc6.js","3263388171436c01f3ce0844777e097b"],["dist/dayjs-locales54.58a62140180335de7dc6.js.map","61acdc71f7365203679dd9103553d992"],["dist/dayjs-locales56.58a62140180335de7dc6.js","627dca08d33d77e71c5caa2250b207f4"],["dist/dayjs-locales56.58a62140180335de7dc6.js.map","3db83c11e2fa4d6e6b2bb20ae8f94bec"],["dist/dayjs-locales58.58a62140180335de7dc6.js","0c88751d4ff5f4045bfff4cdfcd6a579"],["dist/dayjs-locales58.58a62140180335de7dc6.js.map","3e208558390b31c55a4082dfd82601ae"],["dist/dayjs-locales6.58a62140180335de7dc6.js","f94c98a5d2d553e5f751a966ba5b3029"],["dist/dayjs-locales6.58a62140180335de7dc6.js.map","2eccfd01d0f3b7fc654998e4f94a9b16"],["dist/dayjs-locales60.58a62140180335de7dc6.js","848cc48e3e55be529f195fccca9b2516"],["dist/dayjs-locales60.58a62140180335de7dc6.js.map","689b50f2115dd6ce3d8ac270bdfff4b8"],["dist/dayjs-locales62.58a62140180335de7dc6.js","9256ade18ef4cde82005cf83b52669c6"],["dist/dayjs-locales62.58a62140180335de7dc6.js.map","995514e422b98e04fc52cb6de66fec3f"],["dist/dayjs-locales64.58a62140180335de7dc6.js","d325c92c3eb02581fe50cbbbf880db6d"],["dist/dayjs-locales64.58a62140180335de7dc6.js.map","662f0776cfcefef0d78e6f9d62c2dd3c"],["dist/dayjs-locales66.58a62140180335de7dc6.js","5fd6f3355564a7fb12747686448c2bfe"],["dist/dayjs-locales66.58a62140180335de7dc6.js.map","6feaa4849e21634644363f37d97d400c"],["dist/dayjs-locales68.58a62140180335de7dc6.js","426f5ea5228ea38494416b243dfda171"],["dist/dayjs-locales68.58a62140180335de7dc6.js.map","b95b8bcacc1f3ff4c7523135df8ad578"],["dist/dayjs-locales70.58a62140180335de7dc6.js","b52a87c84b213a87796dd811c086bc8a"],["dist/dayjs-locales70.58a62140180335de7dc6.js.map","91657461a65720d3314c9e0d4b9f7a41"],["dist/dayjs-locales72.58a62140180335de7dc6.js","bef26d4d2268ab8c095fa7fb77b34e86"],["dist/dayjs-locales72.58a62140180335de7dc6.js.map","bd710fecb163a89ac09f3530abce2d94"],["dist/dayjs-locales74.58a62140180335de7dc6.js","d6ff5dd464bd80610f1eae80df27a261"],["dist/dayjs-locales74.58a62140180335de7dc6.js.map","8948cbf44b42aa63611bd2812d793819"],["dist/dayjs-locales76.58a62140180335de7dc6.js","90726885c5e6a0f22e3636c3cf8743cd"],["dist/dayjs-locales76.58a62140180335de7dc6.js.map","12779e7bf5bb433a9a356cb218c4734f"],["dist/dayjs-locales78.58a62140180335de7dc6.js","fc3cb4cae2ce4278a477a1ca98c80837"],["dist/dayjs-locales78.58a62140180335de7dc6.js.map","e77a51aec4638dcd999ea9d411020a4e"],["dist/dayjs-locales8.58a62140180335de7dc6.js","5408fbd8c591bcc35e446103c0d5a4d0"],["dist/dayjs-locales8.58a62140180335de7dc6.js.map","ec284ca2faeac4f6e15d2e6a267e0063"],["dist/dayjs-locales80.58a62140180335de7dc6.js","f493148fceb5e28238a40a51b6615b49"],["dist/dayjs-locales80.58a62140180335de7dc6.js.map","072169b2dcfd90c51f8021600513b49f"],["dist/dayjs-locales82.58a62140180335de7dc6.js","9639ed3e0362987dc5f6f271cfcf3a71"],["dist/dayjs-locales82.58a62140180335de7dc6.js.map","6ed0565856ee1de61bf7e9bf56dfe3e4"],["dist/dayjs-locales84.58a62140180335de7dc6.js","1c14d45bcef5299394b2f7654b4ce4f7"],["dist/dayjs-locales84.58a62140180335de7dc6.js.map","834e6394152a9b034142a7b5435f3922"],["dist/dayjs-locales86.58a62140180335de7dc6.js","691c74d3ac6b4a386842c92930079f6d"],["dist/dayjs-locales86.58a62140180335de7dc6.js.map","57efdc550fcb7a701edc3c3f364ed142"],["dist/dayjs-locales88.58a62140180335de7dc6.js","710994b1eb5ac06d1e523625369823f8"],["dist/dayjs-locales88.58a62140180335de7dc6.js.map","ccaac1af93e42d1adae635626b1d0c2a"],["dist/dayjs-locales90.58a62140180335de7dc6.js","141d51cd1e4b8355bf8ecadf5b286755"],["dist/dayjs-locales90.58a62140180335de7dc6.js.map","fd525f1a2367282624354a82b2ed9aa2"],["dist/dayjs-locales92.58a62140180335de7dc6.js","37468e23550fe0cfb2f624bdea300a59"],["dist/dayjs-locales92.58a62140180335de7dc6.js.map","35d645df4a0a9d8e93505c9c52b67d2d"],["dist/dayjs-locales94.58a62140180335de7dc6.js","1cde50ded25c90126122f26b89098c78"],["dist/dayjs-locales94.58a62140180335de7dc6.js.map","ffc60848b2a20a21bad1a4939396d17d"],["dist/dayjs-locales96.58a62140180335de7dc6.js","d5e168af5867b6b429c53ed7beaeba56"],["dist/dayjs-locales96.58a62140180335de7dc6.js.map","3fe6982021fa3c0d1a971112ce8b67d7"],["dist/dayjs-locales98.58a62140180335de7dc6.js","72fdfe63808664038233b54e726ba07f"],["dist/dayjs-locales98.58a62140180335de7dc6.js.map","3823ed78a95cd7ec46af5ac03b8cc558"],["dist/index.amp.html","beb678721ed69b366402ad05e495365a"],["dist/index.basic.html","13671bc02824946ac0ae89e1db5a81ab"],["dist/index.html","7be39c0dab9fd5056af03d516f359dbc"],["dist/index.minimal.html","bc69cfd57592117ce7a79ffb45b4e951"],["dist/manifest.58a62140180335de7dc6.js","f88a879a492112f4bdd0755eafb72b90"],["dist/manifest.58a62140180335de7dc6.js.map","8a25d2b21c3ad1d13a290e177df2c42f"],["dist/vendor.58a62140180335de7dc6.js","d1b7ddfbf46fcc7bfce09c6582fb83f6"],["dist/vendor.58a62140180335de7dc6.js.map","4787a5479919d85cccf13338f39887aa"],["dist/vendors~bodybuilder.58a62140180335de7dc6.js","6b1f2d4d77c1ab0edd491710f2b97642"],["dist/vendors~bodybuilder.58a62140180335de7dc6.js.map","91828d444bc3e7308ade02463a40f08f"],["dist/vendors~vsf-graphql.58a62140180335de7dc6.js","3afe09aa2c1a9e8457530aa0d610a2a4"],["dist/vendors~vsf-graphql.58a62140180335de7dc6.js.map","0c9211dad7e406ccd4f849a3fcbe53dc"],["dist/vsf-category.58a62140180335de7dc6.js","9bd29c45c254ae072c9bc7de4f7af0e5"],["dist/vsf-category.58a62140180335de7dc6.js.map","9915da836ff7fd475fc70eb3dc7515d3"],["dist/vsf-checkout.58a62140180335de7dc6.js","fcb6321800f67008d704b498e79f686b"],["dist/vsf-checkout.58a62140180335de7dc6.js.map","2d650a91eea583e567089ff32d1ad584"],["dist/vsf-checkout~vsf-layout-default~vsf-product.58a62140180335de7dc6.js","4270d24181744922f6844bd0c933810e"],["dist/vsf-checkout~vsf-layout-default~vsf-product.58a62140180335de7dc6.js.map","10673f3d381bd1039f83608a81d067ae"],["dist/vsf-checkout~vsf-my-account.58a62140180335de7dc6.js","707b23cc9c4bb82adade6553233916bb"],["dist/vsf-checkout~vsf-my-account.58a62140180335de7dc6.js.map","edaf6c5d17b8b7169cce44c2b1ec10b5"],["dist/vsf-cms.58a62140180335de7dc6.js","2142d6009c4897ea554d5033616bc032"],["dist/vsf-cms.58a62140180335de7dc6.js.map","f262828c08eaf08c42253788546cc9d1"],["dist/vsf-compare.58a62140180335de7dc6.js","5826618799f7658e49a6c0d5ef842f88"],["dist/vsf-compare.58a62140180335de7dc6.js.map","0c4ca168da4cc976c3ce8f7fa4a23cc7"],["dist/vsf-error.58a62140180335de7dc6.js","e89241306bfa8cb6315b5524ca2d8cd7"],["dist/vsf-error.58a62140180335de7dc6.js.map","ea838b33fd7f2d8118cc1e006e3ab3a2"],["dist/vsf-head-img-banners-de_main-image-json.58a62140180335de7dc6.js","19d63a5bfc1825f7e84d1b8d2ac0a3bd"],["dist/vsf-head-img-banners-de_main-image-json.58a62140180335de7dc6.js.map","98b7e3844cfd91b91586d3df0f75934d"],["dist/vsf-head-img-banners-de_promoted_offers-json.58a62140180335de7dc6.js","ec5e35d69bf942b4defe93ea8cd395ca"],["dist/vsf-head-img-banners-de_promoted_offers-json.58a62140180335de7dc6.js.map","3163e8f64c09bcbd888cddcdf838439b"],["dist/vsf-head-img-banners-it_main-image-json.58a62140180335de7dc6.js","3e009b098e20802dc1c660f1dd4fbba6"],["dist/vsf-head-img-banners-it_main-image-json.58a62140180335de7dc6.js.map","9ef6edcf1148b02043d9bf7ecfd56da5"],["dist/vsf-head-img-banners-it_promoted_offers-json.58a62140180335de7dc6.js","14ba4d421f125727f2b7eafd96213fec"],["dist/vsf-head-img-banners-it_promoted_offers-json.58a62140180335de7dc6.js.map","3ff55aec8acf2cd37f5aac98a303be74"],["dist/vsf-head-img-main-image-json.58a62140180335de7dc6.js","8c67e62638ef21e3b6b1a8f45a867f44"],["dist/vsf-head-img-main-image-json.58a62140180335de7dc6.js.map","1af7dfb6eb89da4b100b3e1db2767234"],["dist/vsf-head-img-promoted_offers-json.58a62140180335de7dc6.js","d618e862f3f3aae1c9313cf2b5483cde"],["dist/vsf-head-img-promoted_offers-json.58a62140180335de7dc6.js.map","6bb1c555f8cc5e807446664781385740"],["dist/vsf-head-img-slider-json.58a62140180335de7dc6.js","92462ede51dbea75806972a1c6eb6ad1"],["dist/vsf-head-img-slider-json.58a62140180335de7dc6.js.map","8a511151a674b2f1eb7ee32c9420689d"],["dist/vsf-home.58a62140180335de7dc6.js","4033530e224d999538cfc0bfefa7b85a"],["dist/vsf-home.58a62140180335de7dc6.js.map","2846dd7988311ac87685bc12c01ce872"],["dist/vsf-languages-modal.58a62140180335de7dc6.js","a742b2657a78c9d42ac30bc303e16bac"],["dist/vsf-languages-modal.58a62140180335de7dc6.js.map","867b9f8acabc19c61d30c83ae6808462"],["dist/vsf-layout-default.58a62140180335de7dc6.js","8f45e4ed676d3c80f30289c846a42ddd"],["dist/vsf-layout-default.58a62140180335de7dc6.js.map","2a46e391143062050662a8fb43d4f5c6"],["dist/vsf-layout-default~vsf-layout-minimal.58a62140180335de7dc6.js","ff7e2a8914b8a610d23b32a41266b83c"],["dist/vsf-layout-default~vsf-layout-minimal.58a62140180335de7dc6.js.map","da1f4e6ac029a53fc545a2f2a921e006"],["dist/vsf-layout-empty.58a62140180335de7dc6.js","93e95cbbd38bce276596abe19eceab4b"],["dist/vsf-layout-empty.58a62140180335de7dc6.js.map","4fd0afe369e52d4b5b8490b77efb1349"],["dist/vsf-layout-minimal.58a62140180335de7dc6.js","e7bef6fe8871c8f7617d63a524456ae1"],["dist/vsf-layout-minimal.58a62140180335de7dc6.js.map","cb6f989459bf7905897c5cceeb641bd6"],["dist/vsf-microcart.58a62140180335de7dc6.js","9d226f0221ede04681384edf3d9eec62"],["dist/vsf-microcart.58a62140180335de7dc6.js.map","7412f67f38a4f9d166135ead104b8e6c"],["dist/vsf-my-account.58a62140180335de7dc6.js","89725b9bc4a8151db8c3fbae6719047a"],["dist/vsf-my-account.58a62140180335de7dc6.js.map","a7c40bd414537eee211ac63fb2a8e9c0"],["dist/vsf-newsletter-modal.58a62140180335de7dc6.js","ed7b22e4884914be6c8c211ad879f8cf"],["dist/vsf-newsletter-modal.58a62140180335de7dc6.js.map","26a7a6d81f35b57f1fa748981fb49614"],["dist/vsf-not-found.58a62140180335de7dc6.js","edeba912ecb8e7cd6d2365da030c2b14"],["dist/vsf-not-found.58a62140180335de7dc6.js.map","f304291f5f85f0a5fd38eb10b9b4a655"],["dist/vsf-order-confirmation.58a62140180335de7dc6.js","eb12c2396d14fd4f1b89c00364e4ac2c"],["dist/vsf-order-confirmation.58a62140180335de7dc6.js.map","e12eb30581ab836738ee4248c50bda2a"],["dist/vsf-product-gallery-carousel.58a62140180335de7dc6.js","c58b135c76b57ac3d442b99d93ed762e"],["dist/vsf-product-gallery-carousel.58a62140180335de7dc6.js.map","35c38121ed91f272294ed677cf93064a"],["dist/vsf-product-gallery-carousel~vsf-product-gallery-zoom-carousel.58a62140180335de7dc6.js","72f132064b738d0a54f3a4d1fe82222e"],["dist/vsf-product-gallery-carousel~vsf-product-gallery-zoom-carousel.58a62140180335de7dc6.js.map","6c5a71c24e47cea31e0968e295aa9318"],["dist/vsf-product-gallery-zoom-carousel.58a62140180335de7dc6.js","695061f52b43952cd17531a6733bf7c7"],["dist/vsf-product-gallery-zoom-carousel.58a62140180335de7dc6.js.map","ad6e0d8e51eba0a1cd2e1a3b671f7af2"],["dist/vsf-product.58a62140180335de7dc6.js","7edf822c6f56bb9ff8c088755c247e4e"],["dist/vsf-product.58a62140180335de7dc6.js.map","07b12d9de3d68ab637b8ace1feacddd5"],["dist/vsf-search-adapter-0.58a62140180335de7dc6.js","693164ba60e2180e1b8c14ca0a15fd9e"],["dist/vsf-search-adapter-0.58a62140180335de7dc6.js.map","7a919a87fa03686edb10bda7217ad9f1"],["dist/vsf-search-adapter-1.58a62140180335de7dc6.js","9bdad156442faabe91d1b16a5e0322f1"],["dist/vsf-search-adapter-1.58a62140180335de7dc6.js.map","472c205c9cdc1fd20ac069e8ed3e5b6f"],["dist/vsf-search-panel.58a62140180335de7dc6.js","720e136f7ddf8f94e06d3ae30365e79c"],["dist/vsf-search-panel.58a62140180335de7dc6.js.map","d607d20cae997e8940ea7ed2ddd8b166"],["dist/vsf-sidebar-menu.58a62140180335de7dc6.js","5936e634d0e001162b00f394733d8be8"],["dist/vsf-sidebar-menu.58a62140180335de7dc6.js.map","419475f6a02a2d817fa6fb30535c83e9"],["dist/vsf-static.58a62140180335de7dc6.js","db20a69d73baa9d8e6463200cf2eab5c"],["dist/vsf-static.58a62140180335de7dc6.js.map","cea2ced7c3314d9fa03051c5c76f3a61"],["dist/vsf-wishlist.58a62140180335de7dc6.js","36078604ccc9afd6fd602c9e4ac4ce23"],["dist/vsf-wishlist.58a62140180335de7dc6.js.map","87ca9fe67cb25fb050e445bd665ca689"],["dist/vue-ssr-client-manifest.json","ce9b18bb423fc4c8ba55eed52b413035"],["dist/wishlist.58a62140180335de7dc6.js","23e39b5b199e999ed6774b1e79e2e753"],["dist/wishlist.58a62140180335de7dc6.js.map","a1803c500610abdce2897733d5ba39fa"]];
var cacheName = 'sw-precache-v3-vue-sfr-' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function(originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function(originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function(originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function(whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function(originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});


// *** Start of auto-included sw-toolbox code. ***
/* 
 Copyright 2016 Google Inc. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/!function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var t;t="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,t.toolbox=e()}}(function(){return function e(t,n,r){function o(c,s){if(!n[c]){if(!t[c]){var a="function"==typeof require&&require;if(!s&&a)return a(c,!0);if(i)return i(c,!0);var u=new Error("Cannot find module '"+c+"'");throw u.code="MODULE_NOT_FOUND",u}var f=n[c]={exports:{}};t[c][0].call(f.exports,function(e){var n=t[c][1][e];return o(n?n:e)},f,f.exports,e,t,n,r)}return n[c].exports}for(var i="function"==typeof require&&require,c=0;c<r.length;c++)o(r[c]);return o}({1:[function(e,t,n){"use strict";function r(e,t){t=t||{};var n=t.debug||m.debug;n&&console.log("[sw-toolbox] "+e)}function o(e){var t;return e&&e.cache&&(t=e.cache.name),t=t||m.cache.name,caches.open(t)}function i(e,t){t=t||{};var n=t.successResponses||m.successResponses;return fetch(e.clone()).then(function(r){return"GET"===e.method&&n.test(r.status)&&o(t).then(function(n){n.put(e,r).then(function(){var r=t.cache||m.cache;(r.maxEntries||r.maxAgeSeconds)&&r.name&&c(e,n,r)})}),r.clone()})}function c(e,t,n){var r=s.bind(null,e,t,n);d=d?d.then(r):r()}function s(e,t,n){var o=e.url,i=n.maxAgeSeconds,c=n.maxEntries,s=n.name,a=Date.now();return r("Updating LRU order for "+o+". Max entries is "+c+", max age is "+i),g.getDb(s).then(function(e){return g.setTimestampForUrl(e,o,a)}).then(function(e){return g.expireEntries(e,c,i,a)}).then(function(e){r("Successfully updated IDB.");var n=e.map(function(e){return t.delete(e)});return Promise.all(n).then(function(){r("Done with cache cleanup.")})}).catch(function(e){r(e)})}function a(e,t,n){return r("Renaming cache: ["+e+"] to ["+t+"]",n),caches.delete(t).then(function(){return Promise.all([caches.open(e),caches.open(t)]).then(function(t){var n=t[0],r=t[1];return n.keys().then(function(e){return Promise.all(e.map(function(e){return n.match(e).then(function(t){return r.put(e,t)})}))}).then(function(){return caches.delete(e)})})})}function u(e,t){return o(t).then(function(t){return t.add(e)})}function f(e,t){return o(t).then(function(t){return t.delete(e)})}function h(e){e instanceof Promise||p(e),m.preCacheItems=m.preCacheItems.concat(e)}function p(e){var t=Array.isArray(e);if(t&&e.forEach(function(e){"string"==typeof e||e instanceof Request||(t=!1)}),!t)throw new TypeError("The precache method expects either an array of strings and/or Requests or a Promise that resolves to an array of strings and/or Requests.");return e}function l(e,t,n){if(!e)return!1;if(t){var r=e.headers.get("date");if(r){var o=new Date(r);if(o.getTime()+1e3*t<n)return!1}}return!0}var d,m=e("./options"),g=e("./idb-cache-expiration");t.exports={debug:r,fetchAndCache:i,openCache:o,renameCache:a,cache:u,uncache:f,precache:h,validatePrecacheInput:p,isResponseFresh:l}},{"./idb-cache-expiration":2,"./options":4}],2:[function(e,t,n){"use strict";function r(e){return new Promise(function(t,n){var r=indexedDB.open(u+e,f);r.onupgradeneeded=function(){var e=r.result.createObjectStore(h,{keyPath:p});e.createIndex(l,l,{unique:!1})},r.onsuccess=function(){t(r.result)},r.onerror=function(){n(r.error)}})}function o(e){return e in d||(d[e]=r(e)),d[e]}function i(e,t,n){return new Promise(function(r,o){var i=e.transaction(h,"readwrite"),c=i.objectStore(h);c.put({url:t,timestamp:n}),i.oncomplete=function(){r(e)},i.onabort=function(){o(i.error)}})}function c(e,t,n){return t?new Promise(function(r,o){var i=1e3*t,c=[],s=e.transaction(h,"readwrite"),a=s.objectStore(h),u=a.index(l);u.openCursor().onsuccess=function(e){var t=e.target.result;if(t&&n-i>t.value[l]){var r=t.value[p];c.push(r),a.delete(r),t.continue()}},s.oncomplete=function(){r(c)},s.onabort=o}):Promise.resolve([])}function s(e,t){return t?new Promise(function(n,r){var o=[],i=e.transaction(h,"readwrite"),c=i.objectStore(h),s=c.index(l),a=s.count();s.count().onsuccess=function(){var e=a.result;e>t&&(s.openCursor().onsuccess=function(n){var r=n.target.result;if(r){var i=r.value[p];o.push(i),c.delete(i),e-o.length>t&&r.continue()}})},i.oncomplete=function(){n(o)},i.onabort=r}):Promise.resolve([])}function a(e,t,n,r){return c(e,n,r).then(function(n){return s(e,t).then(function(e){return n.concat(e)})})}var u="sw-toolbox-",f=1,h="store",p="url",l="timestamp",d={};t.exports={getDb:o,setTimestampForUrl:i,expireEntries:a}},{}],3:[function(e,t,n){"use strict";function r(e){var t=a.match(e.request);t?e.respondWith(t(e.request)):a.default&&"GET"===e.request.method&&0===e.request.url.indexOf("http")&&e.respondWith(a.default(e.request))}function o(e){s.debug("activate event fired");var t=u.cache.name+"$$$inactive$$$";e.waitUntil(s.renameCache(t,u.cache.name))}function i(e){return e.reduce(function(e,t){return e.concat(t)},[])}function c(e){var t=u.cache.name+"$$$inactive$$$";s.debug("install event fired"),s.debug("creating cache ["+t+"]"),e.waitUntil(s.openCache({cache:{name:t}}).then(function(e){return Promise.all(u.preCacheItems).then(i).then(s.validatePrecacheInput).then(function(t){return s.debug("preCache list: "+(t.join(", ")||"(none)")),e.addAll(t)})}))}e("serviceworker-cache-polyfill");var s=e("./helpers"),a=e("./router"),u=e("./options");t.exports={fetchListener:r,activateListener:o,installListener:c}},{"./helpers":1,"./options":4,"./router":6,"serviceworker-cache-polyfill":16}],4:[function(e,t,n){"use strict";var r;r=self.registration?self.registration.scope:self.scope||new URL("./",self.location).href,t.exports={cache:{name:"$$$toolbox-cache$$$"+r+"$$$",maxAgeSeconds:null,maxEntries:null},debug:!1,networkTimeoutSeconds:null,preCacheItems:[],successResponses:/^0|([123]\d\d)|(40[14567])|410$/}},{}],5:[function(e,t,n){"use strict";var r=new URL("./",self.location),o=r.pathname,i=e("path-to-regexp"),c=function(e,t,n,r){t instanceof RegExp?this.fullUrlRegExp=t:(0!==t.indexOf("/")&&(t=o+t),this.keys=[],this.regexp=i(t,this.keys)),this.method=e,this.options=r,this.handler=n};c.prototype.makeHandler=function(e){var t;if(this.regexp){var n=this.regexp.exec(e);t={},this.keys.forEach(function(e,r){t[e.name]=n[r+1]})}return function(e){return this.handler(e,t,this.options)}.bind(this)},t.exports=c},{"path-to-regexp":15}],6:[function(e,t,n){"use strict";function r(e){return e.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}var o=e("./route"),i=e("./helpers"),c=function(e,t){for(var n=e.entries(),r=n.next(),o=[];!r.done;){var i=new RegExp(r.value[0]);i.test(t)&&o.push(r.value[1]),r=n.next()}return o},s=function(){this.routes=new Map,this.routes.set(RegExp,new Map),this.default=null};["get","post","put","delete","head","any"].forEach(function(e){s.prototype[e]=function(t,n,r){return this.add(e,t,n,r)}}),s.prototype.add=function(e,t,n,c){c=c||{};var s;t instanceof RegExp?s=RegExp:(s=c.origin||self.location.origin,s=s instanceof RegExp?s.source:r(s)),e=e.toLowerCase();var a=new o(e,t,n,c);this.routes.has(s)||this.routes.set(s,new Map);var u=this.routes.get(s);u.has(e)||u.set(e,new Map);var f=u.get(e),h=a.regexp||a.fullUrlRegExp;f.has(h.source)&&i.debug('"'+t+'" resolves to same regex as existing route.'),f.set(h.source,a)},s.prototype.matchMethod=function(e,t){var n=new URL(t),r=n.origin,o=n.pathname;return this._match(e,c(this.routes,r),o)||this._match(e,[this.routes.get(RegExp)],t)},s.prototype._match=function(e,t,n){if(0===t.length)return null;for(var r=0;r<t.length;r++){var o=t[r],i=o&&o.get(e.toLowerCase());if(i){var s=c(i,n);if(s.length>0)return s[0].makeHandler(n)}}return null},s.prototype.match=function(e){return this.matchMethod(e.method,e.url)||this.matchMethod("any",e.url)},t.exports=new s},{"./helpers":1,"./route":5}],7:[function(e,t,n){"use strict";function r(e,t,n){return n=n||{},i.debug("Strategy: cache first ["+e.url+"]",n),i.openCache(n).then(function(t){return t.match(e).then(function(t){var r=n.cache||o.cache,c=Date.now();return i.isResponseFresh(t,r.maxAgeSeconds,c)?t:i.fetchAndCache(e,n)})})}var o=e("../options"),i=e("../helpers");t.exports=r},{"../helpers":1,"../options":4}],8:[function(e,t,n){"use strict";function r(e,t,n){return n=n||{},i.debug("Strategy: cache only ["+e.url+"]",n),i.openCache(n).then(function(t){return t.match(e).then(function(e){var t=n.cache||o.cache,r=Date.now();if(i.isResponseFresh(e,t.maxAgeSeconds,r))return e})})}var o=e("../options"),i=e("../helpers");t.exports=r},{"../helpers":1,"../options":4}],9:[function(e,t,n){"use strict";function r(e,t,n){return o.debug("Strategy: fastest ["+e.url+"]",n),new Promise(function(r,c){var s=!1,a=[],u=function(e){a.push(e.toString()),s?c(new Error('Both cache and network failed: "'+a.join('", "')+'"')):s=!0},f=function(e){e instanceof Response?r(e):u("No result returned")};o.fetchAndCache(e.clone(),n).then(f,u),i(e,t,n).then(f,u)})}var o=e("../helpers"),i=e("./cacheOnly");t.exports=r},{"../helpers":1,"./cacheOnly":8}],10:[function(e,t,n){t.exports={networkOnly:e("./networkOnly"),networkFirst:e("./networkFirst"),cacheOnly:e("./cacheOnly"),cacheFirst:e("./cacheFirst"),fastest:e("./fastest")}},{"./cacheFirst":7,"./cacheOnly":8,"./fastest":9,"./networkFirst":11,"./networkOnly":12}],11:[function(e,t,n){"use strict";function r(e,t,n){n=n||{};var r=n.successResponses||o.successResponses,c=n.networkTimeoutSeconds||o.networkTimeoutSeconds;return i.debug("Strategy: network first ["+e.url+"]",n),i.openCache(n).then(function(t){var s,a,u=[];if(c){var f=new Promise(function(r){s=setTimeout(function(){t.match(e).then(function(e){var t=n.cache||o.cache,c=Date.now(),s=t.maxAgeSeconds;i.isResponseFresh(e,s,c)&&r(e)})},1e3*c)});u.push(f)}var h=i.fetchAndCache(e,n).then(function(e){if(s&&clearTimeout(s),r.test(e.status))return e;throw i.debug("Response was an HTTP error: "+e.statusText,n),a=e,new Error("Bad response")}).catch(function(r){return i.debug("Network or response error, fallback to cache ["+e.url+"]",n),t.match(e).then(function(e){if(e)return e;if(a)return a;throw r})});return u.push(h),Promise.race(u)})}var o=e("../options"),i=e("../helpers");t.exports=r},{"../helpers":1,"../options":4}],12:[function(e,t,n){"use strict";function r(e,t,n){return o.debug("Strategy: network only ["+e.url+"]",n),fetch(e)}var o=e("../helpers");t.exports=r},{"../helpers":1}],13:[function(e,t,n){"use strict";var r=e("./options"),o=e("./router"),i=e("./helpers"),c=e("./strategies"),s=e("./listeners");i.debug("Service Worker Toolbox is loading"),self.addEventListener("install",s.installListener),self.addEventListener("activate",s.activateListener),self.addEventListener("fetch",s.fetchListener),t.exports={networkOnly:c.networkOnly,networkFirst:c.networkFirst,cacheOnly:c.cacheOnly,cacheFirst:c.cacheFirst,fastest:c.fastest,router:o,options:r,cache:i.cache,uncache:i.uncache,precache:i.precache}},{"./helpers":1,"./listeners":3,"./options":4,"./router":6,"./strategies":10}],14:[function(e,t,n){t.exports=Array.isArray||function(e){return"[object Array]"==Object.prototype.toString.call(e)}},{}],15:[function(e,t,n){function r(e,t){for(var n,r=[],o=0,i=0,c="",s=t&&t.delimiter||"/";null!=(n=x.exec(e));){var f=n[0],h=n[1],p=n.index;if(c+=e.slice(i,p),i=p+f.length,h)c+=h[1];else{var l=e[i],d=n[2],m=n[3],g=n[4],v=n[5],w=n[6],y=n[7];c&&(r.push(c),c="");var b=null!=d&&null!=l&&l!==d,E="+"===w||"*"===w,R="?"===w||"*"===w,k=n[2]||s,$=g||v;r.push({name:m||o++,prefix:d||"",delimiter:k,optional:R,repeat:E,partial:b,asterisk:!!y,pattern:$?u($):y?".*":"[^"+a(k)+"]+?"})}}return i<e.length&&(c+=e.substr(i)),c&&r.push(c),r}function o(e,t){return s(r(e,t))}function i(e){return encodeURI(e).replace(/[\/?#]/g,function(e){return"%"+e.charCodeAt(0).toString(16).toUpperCase()})}function c(e){return encodeURI(e).replace(/[?#]/g,function(e){return"%"+e.charCodeAt(0).toString(16).toUpperCase()})}function s(e){for(var t=new Array(e.length),n=0;n<e.length;n++)"object"==typeof e[n]&&(t[n]=new RegExp("^(?:"+e[n].pattern+")$"));return function(n,r){for(var o="",s=n||{},a=r||{},u=a.pretty?i:encodeURIComponent,f=0;f<e.length;f++){var h=e[f];if("string"!=typeof h){var p,l=s[h.name];if(null==l){if(h.optional){h.partial&&(o+=h.prefix);continue}throw new TypeError('Expected "'+h.name+'" to be defined')}if(v(l)){if(!h.repeat)throw new TypeError('Expected "'+h.name+'" to not repeat, but received `'+JSON.stringify(l)+"`");if(0===l.length){if(h.optional)continue;throw new TypeError('Expected "'+h.name+'" to not be empty')}for(var d=0;d<l.length;d++){if(p=u(l[d]),!t[f].test(p))throw new TypeError('Expected all "'+h.name+'" to match "'+h.pattern+'", but received `'+JSON.stringify(p)+"`");o+=(0===d?h.prefix:h.delimiter)+p}}else{if(p=h.asterisk?c(l):u(l),!t[f].test(p))throw new TypeError('Expected "'+h.name+'" to match "'+h.pattern+'", but received "'+p+'"');o+=h.prefix+p}}else o+=h}return o}}function a(e){return e.replace(/([.+*?=^!:${}()[\]|\/\\])/g,"\\$1")}function u(e){return e.replace(/([=!:$\/()])/g,"\\$1")}function f(e,t){return e.keys=t,e}function h(e){return e.sensitive?"":"i"}function p(e,t){var n=e.source.match(/\((?!\?)/g);if(n)for(var r=0;r<n.length;r++)t.push({name:r,prefix:null,delimiter:null,optional:!1,repeat:!1,partial:!1,asterisk:!1,pattern:null});return f(e,t)}function l(e,t,n){for(var r=[],o=0;o<e.length;o++)r.push(g(e[o],t,n).source);var i=new RegExp("(?:"+r.join("|")+")",h(n));return f(i,t)}function d(e,t,n){return m(r(e,n),t,n)}function m(e,t,n){v(t)||(n=t||n,t=[]),n=n||{};for(var r=n.strict,o=n.end!==!1,i="",c=0;c<e.length;c++){var s=e[c];if("string"==typeof s)i+=a(s);else{var u=a(s.prefix),p="(?:"+s.pattern+")";t.push(s),s.repeat&&(p+="(?:"+u+p+")*"),p=s.optional?s.partial?u+"("+p+")?":"(?:"+u+"("+p+"))?":u+"("+p+")",i+=p}}var l=a(n.delimiter||"/"),d=i.slice(-l.length)===l;return r||(i=(d?i.slice(0,-l.length):i)+"(?:"+l+"(?=$))?"),i+=o?"$":r&&d?"":"(?="+l+"|$)",f(new RegExp("^"+i,h(n)),t)}function g(e,t,n){return v(t)||(n=t||n,t=[]),n=n||{},e instanceof RegExp?p(e,t):v(e)?l(e,t,n):d(e,t,n)}var v=e("isarray");t.exports=g,t.exports.parse=r,t.exports.compile=o,t.exports.tokensToFunction=s,t.exports.tokensToRegExp=m;var x=new RegExp(["(\\\\.)","([\\/.])?(?:(?:\\:(\\w+)(?:\\(((?:\\\\.|[^\\\\()])+)\\))?|\\(((?:\\\\.|[^\\\\()])+)\\))([+*?])?|(\\*))"].join("|"),"g")},{isarray:14}],16:[function(e,t,n){!function(){var e=Cache.prototype.addAll,t=navigator.userAgent.match(/(Firefox|Chrome)\/(\d+\.)/);if(t)var n=t[1],r=parseInt(t[2]);e&&(!t||"Firefox"===n&&r>=46||"Chrome"===n&&r>=50)||(Cache.prototype.addAll=function(e){function t(e){this.name="NetworkError",this.code=19,this.message=e}var n=this;return t.prototype=Object.create(Error.prototype),Promise.resolve().then(function(){if(arguments.length<1)throw new TypeError;return e=e.map(function(e){return e instanceof Request?e:String(e)}),Promise.all(e.map(function(e){"string"==typeof e&&(e=new Request(e));var n=new URL(e.url).protocol;if("http:"!==n&&"https:"!==n)throw new t("Invalid scheme");return fetch(e.clone())}))}).then(function(r){if(r.some(function(e){return!e.ok}))throw new t("Incorrect response status");return Promise.all(r.map(function(t,r){return n.put(e[r],t)}))}).then(function(){})},Cache.prototype.add=function(e){return this.addAll([e])})}()},{}]},{},[13])(13)});


// *** End of auto-included sw-toolbox code. ***



// Runtime cache configuration, using the sw-toolbox library.

toolbox.router.get("^https://fonts.googleapis.com/", toolbox.cacheFirst, {});
toolbox.router.get("^https://fonts.gstatic.com/", toolbox.cacheFirst, {});
toolbox.router.get("^https://unpkg.com/", toolbox.cacheFirst, {});
toolbox.router.get("/pwa.html", toolbox.networkFirst, {});
toolbox.router.get("/", toolbox.networkFirst, {});
toolbox.router.get("/p/*", toolbox.networkFirst, {});
toolbox.router.get("/c/*", toolbox.networkFirst, {});
toolbox.router.get("/img/(.*)", toolbox.fastest, {});
toolbox.router.get("/api/catalog/*", toolbox.networkFirst, {});
toolbox.router.get("/api/*", toolbox.networkFirst, {});
toolbox.router.get("/assets/logo.svg", toolbox.networkFirst, {});
toolbox.router.get("/index.html", toolbox.networkFirst, {});
toolbox.router.get("/assets/*", toolbox.fastest, {});
toolbox.router.get("/assets/ig/(.*)", toolbox.fastest, {});
toolbox.router.get("/dist/(.*)", toolbox.fastest, {});
toolbox.router.get("/*/*", toolbox.networkFirst, {});
toolbox.router.get("/*/*/*", toolbox.networkFirst, {});
toolbox.router.get("/*", toolbox.networkFirst, {});




importScripts("/dist/core-service-worker.js");

